import { Component, Host, h, State, Event, EventEmitter } from '@stencil/core';
import { nations } from './constants';
import { Nation } from './interfaces';

@Component({
  tag: 'nations-flags',
  styleUrl: 'nations-flags.css',
  shadow: true,
})
export class NationsFlags {
  @State() selectedNation: Nation = nations[0];
  @State() showFlagsMenu: boolean = false;
  @Event() onSelectedNation: EventEmitter<Nation>;

  public selectNation(nation: Nation): void {
    this.selectedNation = nation;
    this.onSelectedNation.emit(nation);
    this.showFlagsMenu = false;
  }

  render() {
    return (
      <Host>
        <div class="nation-flag" id={this.selectedNation.id} onClick={() => {
          this.showFlagsMenu = true;
        }}>
          <img src={this.selectedNation.img}/>
        </div><br/>
        {this.showFlagsMenu ? this.renderMenu() : ''}
      </Host>
    );
  }

  renderMenu() {
    return (
      <div class="grid-container" id="flags">
        {nations.map((nation) => {
          return (
            <div id={nation.id} onClick={() => {
              this.selectNation(nation);
            }}>
              <img class="nation-flag" src={nation.img} />
            </div>
          );
        })}
      </div>
    );
  }

}
