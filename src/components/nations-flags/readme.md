# nations-flags



<!-- Auto Generated Below -->


## Events

| Event              | Description | Type                  |
| ------------------ | ----------- | --------------------- |
| `onSelectedNation` |             | `CustomEvent<Nation>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
