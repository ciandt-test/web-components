export interface Nation {
  id: string;
  name: string;
  img: string;
}
