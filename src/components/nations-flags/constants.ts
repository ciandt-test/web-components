import { Nation } from './interfaces';
import brazilFlag from './icons/brasil.svg';
import franceFlag from './icons/franca.svg';
import ukFlag from './icons/reino-unido.svg';
import germanyFlag from './icons/alemanha.svg';
import chinaFlag from './icons/china.svg';
import italyFlag from './icons/italia.svg';
import spainFlag from './icons/espanha.svg';
import japanFlag from './icons/japao.svg';
import usaFlag from './icons/estados-unidos.svg';

export const nations = [
  { id: '0', name: 'Brazil', img: brazilFlag },
  { id: '1', name: 'France', img: franceFlag },
  { id: '2', name: 'United Kingdom', img: ukFlag },
  { id: '3', name: 'Germany', img: germanyFlag },
  { id: '4', name: 'China', img: chinaFlag },
  { id: '5', name: 'Italy', img: italyFlag },
  { id: '6', name: 'Spain', img: spainFlag },
  { id: '7', name: 'Japan', img: japanFlag },
  { id: '8', name: 'United States', img: usaFlag },
] as Nation[];
